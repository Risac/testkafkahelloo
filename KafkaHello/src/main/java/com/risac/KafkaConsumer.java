package com.risac;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;

public class KafkaConsumer {

	private static final Logger logger = LogManager.getLogger(KafkaProducer.class.getName());

	@KafkaListener(topics = "TOPIC-DEMO")
	public void consume(String message) {
		logger.info("Consuming Message {}", message);
	}

}
